﻿select bovino_id, id_trajetoria, descricao, count(ponto) as pontos, tempo, comprimento, velocidade_media, data_hora_inicio, data_hora_fim from trajesim_pontos inner join trajesim_trajetorias on id_trajetoria = id_trajetoria group by id_trajetoria order by id_trajetoria;
select trajesim_trajetorias.id_trajetoria, trajesim_trajetorias.descricao, count(trajesim_pontos.id_ponto) as pontos
from trajesim_trajetorias inner join trajesim_pontos on trajesim_trajetorias.id_trajetoria = trajesim_pontos.id_trajetoria_id group by trajesim_trajetorias.id_trajetoria order by trajesim_trajetorias.id_trajetoria;

-- YX ::: ID Bovino, ID Trajetória, Descricao da Trajetória, Tempo de Duração, Comprimento, Pontos, Velocidade Média, Data e Hora Inicial, Data e Hora Final
SELECT t.id_trajetoria, count(p.id_ponto) as qtd_pontos, (t.data_hora_fim - t.data_hora_inicio) as tempo_duracao, ST_Length(t.trajetoria_yx)*3.6 as comprimento_km, min(p.velocidade) as velocidade_min_km, max(p.velocidade) as velocidade_max_km,
(ST_Length(t.trajetoria_yx)/EXTRACT(EPOCH FROM (t.data_hora_fim-t.data_hora_inicio)))*3.6 as vm_kmh
FROM trajesim_trajetorias as t inner join trajesim_pontos as p on t.id_trajetoria = p.id_trajetoria_id group by t.id_trajetoria order by t.id_trajetoria


-- XY ::: ID Bovino, ID Trajetória, Descricao da Trajetória, Tempo de Duração, Comprimento, Pontos, Velocidade Média, Data e Hora Inicial, Data e Hora Final
SELECT t.id_trajetoria, count(p.id_ponto) as qtd_pontos, (t.data_hora_fim - t.data_hora_inicio) as tempo_duracao, ST_Length(t.trajetoria)*3.6 as comprimento_km, min(p.velocidade) as velocidade_min_km, max(p.velocidade) as velocidade_max_km,
(ST_Length(t.trajetoria)/EXTRACT(EPOCH FROM (t.data_hora_fim-t.data_hora_inicio)))*3.6 as vm_kmh
FROM trajesim_trajetorias as t inner join trajesim_pontos as p on t.id_trajetoria = p.id_trajetoria_id group by t.id_trajetoria order by t.id_trajetoria

-- tempo de duração
select (data_hora_fim - data_hora_inicio), EXTRACT(EPOCH FROM (data_hora_fim-data_hora_inicio))from trajesim_trajetorias

-- tempo em segundos
select EXTRACT(EPOCH FROM (data_hora_fim-data_hora_inicio)) from trajesim_trajetorias


-- comprimento metros
select ST_Length(trajetoria) as m, ST_Length(trajetoria)*3.6 as km from trajesim_trajetorias

-- quantidade de pontos
select id_trajetoria_id, count(id_ponto) from trajesim_pontos group by id_trajetoria_id order by id_trajetoria_id

-- velocidade foi cadastrada em km/h
-- velocidade mínima
select min(velocidade) from trajesim_pontos group by id_trajetoria_id order by id_trajetoria_id

-- velocidade máxima
select max(velocidade) from trajesim_pontos group by id_trajetoria_id order by id_trajetoria_id

-- velocidade média
select (ST_Length(trajetoria)/EXTRACT(EPOCH FROM (data_hora_fim-data_hora_inicio))) as m_s, (ST_Length(trajetoria)/EXTRACT(EPOCH FROM (data_hora_fim-data_hora_inicio)))*3.6 as k_h from trajesim_trajetorias

